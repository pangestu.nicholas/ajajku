from django.test import TestCase,Client,LiveServerTestCase
from django.urls import resolve
from .views import home
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import unittest
import time


# Create your tests here.
class unitTest(TestCase):
    def test_url_status_code(self):
        response=Client().get("/")
        self.assertEqual(response.status_code,200)

    def test_template_used_in_app(self):
        response=Client().get("/")
        self.assertTemplateUsed(response,"home.html")

    def test_func_used(self):
        response=resolve("/")
        self.assertEqual(response.func,home)

class fungtional_testt(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_website_element_exist(self):
        self.browser.get("http://localhost:8000/")
        self.assertIn("Lab 8",self.browser.page_source)
        self.assertIn("Silahkan",self.browser.page_source)
        self.browser.find_element_by_name("search").send_keys("calculus")
        self.browser.find_element_by_id("gambar").click()
        time.sleep(3)
        self.assertIn("Ken Binmore",self.browser.page_source)

    